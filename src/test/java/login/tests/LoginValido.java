package login.tests;

import config.Ambiente;
import config.Config;
import config.User;
import login.action.ActionLogin;
import login.validations.ValidationComAlert;
import login.validations.Validations_LoginValido;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class LoginValido {
    private WebDriver driver;
    private User user;

    @BeforeClass
    public void criarDriver() {
        Config webdriver = new Config();
        driver = webdriver.webdriver(Ambiente.HOMOLOG);
        user = User.getUser("master1");
    }

    @AfterClass
    public void fecharDriver() {
//        driver.quit();
    }

    @Test(priority = 1, groups = "login")
    public void Dado_acesso_paginaWeb_Attornatus() {
        // Nada a fazer aqui, pois já é feito no @BeforeClass
    }

    @Test(priority = 2, groups = "login", dependsOnMethods = "Dado_acesso_paginaWeb_Attornatus")
    public void Quando_inserir_email_e_senha_Validos() throws InterruptedException {
        ActionLogin actions = new ActionLogin(driver);

        actions.AcessarSistema();
        actions.preencherUsuario(user.getEmail());
        actions.preencherSenha(user.getPassword());
        actions.clicarNoBotaoEntrar();
    }

    @Test(priority = 3, groups = "login", dependsOnMethods = "Quando_inserir_email_e_senha_Validos")
    public void Entao_loginSeraRealizado_comSucesso() {
        Validations_LoginValido validations = new Validations_LoginValido(driver);
        ValidationComAlert alert = new ValidationComAlert(driver);

//      validations.validarLogin_Sucesso();
        alert.modalAlert();

    }
}