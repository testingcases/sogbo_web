package login.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class PageLogin {
    private final WebDriver driver;

    public PageLogin(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    //Mapping login
    @FindBy(how = How.XPATH, using = "/html/body/div/div/main/div/div/section[1]/div/footer/button")
    public WebElement btnAcessarSistema;
    @FindBy(how = How.NAME, using = "usuarionome")
    public WebElement user;

    @FindBy(how = How.NAME, using = "senha")
    public WebElement password;

    @FindBy(how = How.XPATH, using = "/html/body/div/div/div/div[2]/form/div[4]/input")
    public WebElement entrar;
}