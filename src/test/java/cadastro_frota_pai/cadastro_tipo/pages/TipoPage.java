package cadastro_tipo.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class TipoPage {
    private final WebDriver driver;

    public TipoPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    //Mapping Criar Frota
    @FindBy(how = How.XPATH, using = "/html/body/div/aside/section/ul/li[1]/ul/li[1]/ul/li[2]/a")
    public WebElement ClicarModuloTipo;
    @FindBy(how = How.XPATH, using = "//*[@id=\"btn-create\"]")
    public WebElement btn_AdicionarTipo;
}



