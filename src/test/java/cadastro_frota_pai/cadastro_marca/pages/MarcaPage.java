package cadastro_frota_pai.cadastro_marca.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class MarcaPage {
    private final WebDriver driver;

    public MarcaPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    //Mapping Criar Frota
    @FindBy(how = How.XPATH, using = "/html/body/div/aside/section/ul/li[1]/ul/li[1]/ul/li[3]/a")
    public WebElement BtnModuloMarca;
    @FindBy(how = How.XPATH, using = "//*[@id=\"btn-create\"]/span")
    public WebElement btn_AdicionarMarca;
}



