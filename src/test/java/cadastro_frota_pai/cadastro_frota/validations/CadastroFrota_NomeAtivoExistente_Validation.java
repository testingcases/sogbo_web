//package controle_frota.validations;
//
//import org.openqa.selenium.By;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
//import org.openqa.selenium.support.PageFactory;
//import org.openqa.selenium.support.ui.ExpectedConditions;
//import org.openqa.selenium.support.ui.WebDriverWait;
//import org.testng.Assert;
//
//public class CadastroFrota_NomeAtivoExistente_Validation {
//    private final WebDriver driver;
//
//    public CadastroFrota_NomeAtivoExistente_Validation(WebDriver driver) {
//        PageFactory.initElements(driver, this);
//        this.driver = driver;
//    }

//    public void Validar_MensagemApresentada_NoCadastro_ComNomeExistente(String mensagemEsperada) {
//        By xpathResultData = By.xpath("/html/body/div/div[1]/section[2]/div[2]/div[1]");
//
//        // Aguarde até que o elemento esteja presente na página
//        WebDriverWait wait = new WebDriverWait(driver, 10);
//        WebElement elementoResultData = wait.until(ExpectedConditions.presenceOfElementLocated(xpathResultData));
//
//        // Verifique se o texto do elemento é exatamente igual à mensagem esperada
//        String textoElemento = elementoResultData.getText().trim();
//        if (textoElemento.equals(mensagemEsperada)) {
//            System.out.println("Teste passou - Mensagem esperada apresentada: ");
//            System.out.println(textoElemento);
//        } else {
//            System.out.println("Teste falhou - Mensagem diferente da esperada. Mensagem esperada:");
//            System.out.println(mensagemEsperada);
//            System.out.println("Mensagem encontrada: " + textoElemento);
//        }
//    }
//}

package cadastro_frota.validations;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class CadastroFrota_NomeAtivoExistente_Validation {
    private final WebDriver driver;

    public CadastroFrota_NomeAtivoExistente_Validation(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    public void Validar_MensagemApresentada_NoCadastro_ComNomeExistente() {
        By xpathResultData = By.xpath("/html/body/div/div[1]/section[2]/div[2]/div[1]");

        WebDriverWait wait = new WebDriverWait(driver, 10);
        WebElement elementoResultData = wait.until(ExpectedConditions.presenceOfElementLocated(xpathResultData));

        // Imprimir o textoElemento no terminal
        String textoElemento = elementoResultData.getText().trim();
        System.out.println("Texto Elemento: " + textoElemento);

        // Frase a ser verificada
        String fraseEsperada = "já encontra-se cadastrado";
//        String fraseEsperada = "tim vivo claro";


        try {
            // Verificar se o textoElemento contém a frase esperada
            Assert.assertTrue(textoElemento.contains(fraseEsperada),
                    "A frase esperada '" + fraseEsperada + "' não foi encontrada em: " + textoElemento);

            // Imprimir resultado no terminal
            System.out.println("Teste passou - A frase esperada foi encontrada: " + fraseEsperada);
        } catch (AssertionError e) {
            // Em caso de falha na verificação
            System.out.println("Teste falhou - A frase esperada não foi encontrada: " + fraseEsperada);
            // Repassar a exceção para que o teste seja marcado como falho no relatório do TestNG
            throw e;
        }
    }
}