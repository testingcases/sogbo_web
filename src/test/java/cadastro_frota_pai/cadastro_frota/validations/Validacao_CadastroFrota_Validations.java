package cadastro_frota.validations;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class Validacao_CadastroFrota_Validations {
    private final WebDriver driver;

    public Validacao_CadastroFrota_Validations(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    public void validarFrotaCriadaComSucesso(String nomeFrota) {
        By xpathResultData = By.xpath("//*[@id='resultdata']/tbody/tr/td[4]");
        By xpathErro = By.xpath("//div[@class='mensagem-erro' and contains(text(), 'Esse nome de Frota já encontra-se cadastrado, verifique pois esta Frota pode estar entre as excluídas!!')]");

        WebDriverWait wait = new WebDriverWait(driver, 10);

        // Aguarde até que o elemento de sucesso ou a mensagem de erro estejam presentes
        WebElement elementoResultData = wait.until(ExpectedConditions.presenceOfElementLocated(xpathResultData));
        WebElement elementoErro = wait.until(ExpectedConditions.presenceOfElementLocated(xpathErro));

        // Verifique se o elemento de sucesso está visível
        if (elementoResultData.isDisplayed()) {
            System.out.println("Teste passou - Frota criada com sucesso. Nome da Frota: " + nomeFrota);
        } else {
            // Verifique se a mensagem de erro específica é apresentada
            Assert.assertTrue(elementoErro.isDisplayed(), "Teste falhou - A frota foi criada, mas a mensagem de erro esperada não foi apresentada.");
            System.out.println("Teste passou - Mensagem de erro apresentada corretamente: " + elementoErro.getText());
        }
    }
}
