package cadastro_frota_pai.cadastro_frota.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class FrotaPage {
    private final WebDriver driver;

    public FrotaPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    //Mapping Criar Frota
    @FindBy(how = How.XPATH, using = "//*[@id=\"resultdata\"]/tbody/tr[5]/td[1]/button")
    public WebElement bntCadastrarFrota;
    @FindBy(how = How.XPATH, using = "/html/body/div/header/nav/a")
    public WebElement expandirMenuPrincipal;
    @FindBy(how = How.XPATH, using = "/html/body/div/aside/section/ul/li[1]/ul/li[1]/a")
    public WebElement expandirFrota;
    @FindBy(how = How.XPATH, using = "/html/body/div/aside/section/ul/li[1]/a/span[1]")
    public WebElement expandirMenuCadastros;
    @FindBy(how = How.XPATH, using = "/html/body/div/aside/section/ul/li[1]/ul/li[1]/ul/li[1]/a")
    public WebElement clicarNoBotaoFrota;
    @FindBy(how = How.XPATH, using = "//*[@id=\"btn-create\"]/span")
    public WebElement btnAdicionarFrota;
    @FindBy(how = How.ID, using = "nome")
    public WebElement nomeFrota;
    @FindBy(how = How.NAME, using = "observacao")
    public WebElement observacoes;
    @FindBy(how = How.XPATH, using = "/html/body/div/div[1]/section[2]/div[2]/div/div/div/div/div/form/div[4]/div/div/button[1]")
    public WebElement btnConfirmarCadsatroFrota;

    @FindBy(how = How.XPATH, using = "//button[@type='submit']")
    public WebElement btnConfirmarCadastroGeral;

    @FindBy(how = How.XPATH, using = "//*[@id=\"btn-return\"]/span")
    public WebElement imagem;


    //Mapping Editar Frota

    @FindBy(how = How.XPATH, using = "//*[@id=\"resultdata\"]/tbody/tr/td[1]/button[2]/span")
    public WebElement ClicarNoBotaoEditar;
    @FindBy(how = How.XPATH, using = "/html/body/div/div[1]/section[2]/div[2]/div/div/div/div/div/form/div[5]/div/div/button[1]/span")
    public WebElement btnConfirmarEdicaoFrota;



    //Mapping Excluir Frota
    @FindBy(how = How.XPATH, using = "//*[@id=\"resultdata_filter\"]/label/input")
    public WebElement EditarNomeFrota;
    @FindBy(how = How.XPATH, using = "//*[@id=\"resultdata\"]/tbody/tr/td[1]/button[3]")
    public WebElement btnExcluir;
    @FindBy(how = How.XPATH, using = "/html/body/div/div[1]/section[2]/div[2]/div/div/div/div/div/form/div[5]/div/div/button[1]")
    public WebElement confirmarExclusao;

}
