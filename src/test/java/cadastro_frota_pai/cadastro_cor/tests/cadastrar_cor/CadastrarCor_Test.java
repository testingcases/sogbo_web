package cadastro_frota_pai.cadastro_cor.tests.cadastrar_cor;


import cadastro_frota.validations.Scroll;
import cadastro_frota_pai.cadastro_cor.actions.CorActions;
import cadastro_frota_pai.cadastro_frota.actions.FrotaActions;
import config.Ambiente;
import config.Config;
import config.User;
import login.action.ActionLogin;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class CadastrarCor_Test {
    private WebDriver driver;
    private User user;
    //String nomeFrota = "Frota Freitas";
    String nomeModelo = "Modelo Test";
    String informacoesModelo = "Modelo criado por automação2";


    @BeforeClass
    public void criarDriver() {
        Config webdriver = new Config();
        driver = webdriver.webdriver(Ambiente.HOMOLOG);
        user = User.getUser("master1");
    }

    @AfterClass
    public void fecharDriver() {
        // driver.quit();
    }

    @Test(priority = 1, groups = "login")
    public void Dado_acesso_paginaWeb_SOGBOWEB() {
        // Nada a fazer aqui, pois já é feito no @BeforeClass
    }

    @Test(priority = 2, groups = "login", dependsOnMethods = "Dado_acesso_paginaWeb_SOGBOWEB")
    public void Quando_inserir_email_e_senha_Validos() throws InterruptedException {
        ActionLogin actions = new ActionLogin(driver);

        actions.AcessarSistema();
        actions.preencherUsuario(user.getEmail());
        actions.preencherSenha(user.getPassword());
        actions.clicarNoBotaoEntrar();
    }

    @Test(priority = 3, groups = "marca", dependsOnMethods = "Quando_inserir_email_e_senha_Validos")
    public void E_Acesar_O_Modulo_Marca() {
        FrotaActions frota = new FrotaActions(driver);
        CorActions cor  = new CorActions(driver);

        frota.CaminhoPara_CriarEditarExcluir_Frota();
        cor.ClicarModuloModelo();
        cor.BtnAdicionarModelo();

    }

    @Test(priority = 4, groups = "marca", dependsOnMethods = "E_Acesar_O_Modulo_Marca")
    public void Entao_DeverSerPossivel_CadastrarUmaFrota_ComSucesso() {
        FrotaActions frota = new FrotaActions(driver);
        Scroll scrol = new Scroll(driver);

        frota.InserirInformacoesDaFrota(nomeModelo, informacoesModelo);
        scrol.scrollModalDown(100);
        frota.BtnConfirmar_Cadastro_Geral();// Componente Global Extensão cara cadastro de tipo



       // frota.BtnAdicionarFrota(); // Componente Global Extensão cara cadastro de tipo
    }
//
//    @Test(priority = 5, groups = "Frota", dependsOnMethods = "Entao_DeverSerPossivel_CadastrarUmaFrota_ComSucesso")
//    public void Validar_SeFrota_FoiCriada_ComSucesso() throws InterruptedException {
//        FrotaActions frota = new FrotaActions(driver);
//        Validacao_CadastroFrota_Validations validacao = new Validacao_CadastroFrota_Validations(driver);
//
//        frota.PesquisarNomeDaFrota(nomeFrota);
//        Thread.sleep(2000);
//        validacao.validarFrotaCriadaComSucesso(nomeFrota);
    }

