package cadastro_frota_pai.cadastro_veiculo.tests.cadastrar_veiculo;


import cadastro_frota.validations.Scroll;
import cadastro_frota_pai.cadastro_frota.actions.FrotaActions;
import cadastro_frota_pai.cadastro_veiculo.actions.VeiculoActions;
import config.Ambiente;
import config.Config;
import config.User;
import login.action.ActionLogin;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class CadastrarVeiculo_Test {
    private WebDriver driver;
    private User user;
    //String nomeVeiculo = "Frota Freitas";
    String nomeVeiculo = "Veiculo Liberts";
    String informacoesVeiculo = "Veiculo";


    @BeforeClass
    public void criarDriver() {
        Config webdriver = new Config();
        driver = webdriver.webdriver(Ambiente.HOMOLOG);
        user = User.getUser("master1");
    }

    @AfterClass
    public void fecharDriver() {
        // driver.quit();
    }

    @Test(priority = 1, groups = "login")
    public void Dado_acesso_paginaWeb_SOGBOWEB() {
        // Nada a fazer aqui, pois já é feito no @BeforeClass
    }

    @Test(priority = 2, groups = "login", dependsOnMethods = "Dado_acesso_paginaWeb_SOGBOWEB")
    public void Quando_inserir_email_e_senha_Validos() throws InterruptedException {
        ActionLogin actions = new ActionLogin(driver);

        actions.AcessarSistema();
        actions.preencherUsuario(user.getEmail());
        actions.preencherSenha(user.getPassword());
        actions.clicarNoBotaoEntrar();


    }

    @Test(priority = 3, groups = "veiculo", dependsOnMethods = "Quando_inserir_email_e_senha_Validos")
    public void E_Acesar_O_Modulo_Veiculo() {
        FrotaActions frota = new FrotaActions(driver);
        VeiculoActions veiculo = new VeiculoActions(driver);

        frota.CaminhoPara_CriarEditarExcluir_Frota();
        veiculo.ClicarNoModuloVeiculo();
        veiculo.BtnAdicionarVeiculo();
    }

    @Test(priority = 4, groups = "tipo", dependsOnMethods = "E_Acesar_O_Modulo_Veiculo")
    public void Entao_DeverSerPossivel_CadastrarUmVeiculo_ComSucesso() {
        FrotaActions frota = new FrotaActions(driver);
        Scroll scrol = new Scroll(driver);

        //frota.InserirInformacoesDaFrota(nomeVeiculo, informacoesVeiculo);
        //scrol.scrollModalDown(100);
        //frota.BtnConfirmar_Cadastro_Geral();
    }
//
//    @Test(priority = 5, groups = "Frota", dependsOnMethods = "Entao_DeverSerPossivel_CadastrarUmaFrota_ComSucesso")
//    public void Validar_SeFrota_FoiCriada_ComSucesso() throws InterruptedException {
//        FrotaActions frota = new FrotaActions(driver);
//        Validacao_CadastroFrota_Validations validacao = new Validacao_CadastroFrota_Validations(driver);
//
//        frota.PesquisarNomeDaFrota(nomeFrota);
//        Thread.sleep(2000);
//        validacao.validarFrotaCriadaComSucesso(nomeFrota);
    }

