package cadastro_frota_pai.cadastro_acessorios.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class AcessoriosPage {
    private final WebDriver driver;

    public AcessoriosPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    //Mapping Criar Frota
    @FindBy(how = How.XPATH, using = "/html/body/div/aside/section/ul/li[1]/ul/li[1]/ul/li[9]/a")
    public WebElement BtnModuloAcessorios;
    @FindBy(how = How.XPATH, using = "//*[@id=\"btn-create\"]/span")
    public WebElement btn_AdicionarAcessorios;
}



